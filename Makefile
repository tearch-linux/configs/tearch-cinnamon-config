DESTDIR=/

all: install

install:
	mkdir -p $(DESTDIR)/usr/share/glib-2.0/schemas/ || true
	mkdir -p $(DESTDIR)/etc/skel/.cinnamon || true

	cp -prfv schemas/* $(DESTDIR)/usr/share/glib-2.0/schemas/
	cp -prfv skel/* $(DESTDIR)/etc/skel/.cinnamon